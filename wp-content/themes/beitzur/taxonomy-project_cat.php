<?php
get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = new WP_Query([
		'posts_per_page' => 4,
		'post_type' => 'project',
		'suppress_filters' => false,
		'tax_query' => array(
				array(
						'taxonomy' => 'project_cat',
						'field' => 'term_id',
						'terms' => $query->term_id,
				)
		)
]);
$published_posts = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => 'project',
		'suppress_filters' => false,
		'tax_query' => array(
				array(
						'taxonomy' => 'project_cat',
						'field' => 'term_id',
						'terms' => $query->term_id,
				)
		)
]);
$topImg = get_field('top_img', $query);
?>

<article class="page-body mb-4 blog-body">
	<?php get_template_part('views/partials/content', 'top', [
		'img' => $topImg ? $topImg['url'] : IMG.'top-gallery.png',
		'title' => $query->name,
	]);
	?>
	<div class="container">
		<div class="row justify-content-center">
			<?php if ($title = get_field('title', $query)) : ?>
				<div class="col-12">
					<h1 class="with-line-title with-line-white"><?= $title; ?></h1>
				</div>
			<?php endif; ?>
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && count($published_posts->posts) > 8) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="project" data-taxtype="project_cat" data-term="<?= $query->term_id; ?>">
						<img src="<?= ICONS ?>arrow-down.png" alt="load-more">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="inverse-repeat-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $slider,
			'img' => get_field('slider_img', $query),
	]);
}
if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $faq,
			'subtitle' => get_field('faq_title_bold', $query),
			'title' => get_field('faq_title', $query),
	]);
}
get_footer(); ?>

