<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$fax = opt('fax');
?>
<footer>
	<div class="footer-main">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ($f_title = opt('foo_form_title')) : ?>
						<h2 class="base-form-title"><?= $f_title; ?></h2>
					<?php endif;
					if ($f_subtitle = opt('foo_form_subtitle')) : ?>
						<p class="base-form-subtitle mb-4"><?= $f_subtitle; ?></p>
					<?php endif;
					getForm('32'); ?>
				</div>
			</div>
		</div>
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png">
			<h4 class="top-text mt-2">
				חזרה
			</h4>
			<h4 class="top-text mt-2">
				למעלה
			</h4>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-sm-between justify-content-center align-items-start foo-after-title">
				<div class="col-lg-auto col-6 foo-menu">
					<h3 class="foo-title">
						מפת אתר
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-12 foo-menu foo-links-menu">
					<h3 class="foo-title">
						<?php $foo_l_title = opt('foo_menu_links_title');
						echo $foo_l_title ? $foo_l_title : 'מאמרים פופולריים'; ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-6 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						צור קשר
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<?= 'טלפון: '.$tel; ?>
									</a>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<?= 'אימייל: '.$mail; ?>
									</a>
								</li>
							<?php endif;
							if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<?= 'כתובת: '.$address; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
						<div>
							<h3 class="foo-title font-weight-normal">
								עקבו אחרינו
							</h3>
							<div class="d-flex align-items-center">
								<?php if ($instagram = opt('instagram')) : ?>
									<a href="<?= $instagram; ?>" class="social-link-item ml-2">
										<img src="<?= ICONS ?>instagram.png" alt="instagram">
									</a>
								<?php endif;
								if ($facebook = opt('facebook')) : ?>
									<a href="<?= $facebook; ?>" class="social-link-item">
										<img src="<?= ICONS ?>facebook.png" alt="facebook">
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
