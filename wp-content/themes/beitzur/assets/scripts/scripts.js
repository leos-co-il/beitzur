(function($) {
	// document.body.classList.remove('no-js');
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	var width = $(window).width();
	if(width > 768){
		$('.menu-item-has-children').hover(function(){
			$(this).children('.sub-menu').fadeIn();
		}, function(){
			$(this).children('.sub-menu').fadeOut();
		});
	}
	//Load more items
	var size_li = $('#success-block .gallery-chunk').size();
	var x = 1;
	$('#success-block .gallery-chunk:lt('+x+')').addClass('show');
	$('#load-more-items').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('#success-block .gallery-chunk:lt('+x+')').addClass('show');
		if (x === size_li) {
			$('#load-more-items').addClass('hide');
		}
	});
	$( document ).ready(function() {
		// $('.post-card').hover(function (){
		// 	$(this).children('.post-img').children('.card-content-wrap').children('.post-card-content').slideFadeToggle();
		// });
		$('#project-send').click(function () {
			var proUrl = $('.project-form-wrap').data('link');
			var proId = $('.project-form-wrap').data('id');
			$('#project-input').html(proUrl);
			$('#project-input-id').html(proId);
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
			autoplay: true,
			autoplaySpeed: 3000,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			// rtl: true,
			asNavFor: '.gallery-slider',
			dots: true,
			arrows: false,
			vertical: true,
			verticalSwiping: true,
			// responsive: [
			// 	{
			// 		breakpoint: 576,
			// 		settings: {
			// 			slidesToShow: 2,
			// 		}
			// 	},
			// ]
		});
		$('.projects-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').addClass('active-faq');
			show.parent().children('.question-title').children('.arrow-top').addClass('hide');
			show.parent().children('.question-title').children('.arrow-bottom').addClass('show');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').removeClass('active-faq');
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('hide');
			collapsed.parent().children('.question-title').children('.arrow-bottom').removeClass('show');
		});
	});

	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading mx-2"><i class="fas fa-spinner fa-pulse"></i></div>');
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var taxType = $(this).data('taxtype');
		var type = $(this).data('type');
		var page = $(this).data('page');
		var quantity = $('.more-card').length;

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				// postType: postType,
				type: type,
				termID: termID,
				ids: ids,
				quantity: quantity,
				page: page,
				taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );

