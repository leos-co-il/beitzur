<?php

the_post();
get_header();
$fields = get_fields();
?>

<article class="page-body">
	<?php get_template_part('views/partials/content', 'top', [
		'img' => $fields['top_img'] ? $fields['top_img']['url'] : IMG.'top-post.png',
		'title' => 'בלוג',
	]);
	?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="with-line-title with-line-white"><?php the_title(); ?></h1>
			</div>
			<div class="col-12 post-content-col">
				<div class="post-image-wrap">
					<?php if (has_post_thumbnail()) : ?>
						<img src="<?= postThumb(); ?>" alt="post-image" class="post-thumb-main">
					<?php endif; ?>
					<div class="post-form-wrap black-inputs">
						<?php if ($f_title = opt('post_form_title')) : ?>
							<h3 class="post-form-title"><?= $f_title; ?></h3>
						<?php endif;
						if ($f_subtitle = opt('post_form_subtitle')) : ?>
							<h4 class="post-form-subtitle"><?= $f_subtitle; ?></h4>
						<?php endif;
						getForm('35'); ?>
					</div>
				</div>
				<div class="base-output post-output post-order-content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
$link_all = $fields['same_posts_link'] ? $fields['same_posts_link'] : opt('all_posts_link');
if ($samePosts) : ?>
	<section class="same-posts">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="with-line-title with-line-white">
						<?= $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים שעושיים לעניין אותך'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
			<?php if ($link_all) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $link_all['url']; ?>" class="base-link base-link-reverse">
							<?= (isset($link_all['title']) && $link_all['title']) ?
									$link_all['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
