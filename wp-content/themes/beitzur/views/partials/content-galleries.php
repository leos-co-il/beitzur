<?php if (isset($args['gals']) && $args['gals']) : ?>
	<div class="row justify-content-center align-items-stretch">
		<?php foreach ($args['gals'] as $link) : ?>
			<div class="col-sm-3 col-6 col-no-pads">
				<a href="<?= get_the_permalink($link); ?>" class="gallery-link">
					<span class="first-gallery-title"><?= text_preview($link->post_title, 1, ''); ?></span>
					<span class="all-gallery-title"><?= substr(strstr($link->post_title," "), 1);?></span>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
