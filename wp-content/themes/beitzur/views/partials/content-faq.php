<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container faq-back pencil-relative-block">
			<img src="<?= IMG ?>pencil.png" alt="pencil" class="contact-pencil">
			<div class="row justify-content-start">
				<div class="col-xl-9 col-lg-10 col-12">
					<div class="title-wrap-faq">
						<h2 class="faq-title-main font-weight-normal">
							<?= (isset($args['faq_title']) && $args['faq_title']) ? $args['faq_title'] : 'יש לכם שאלה?'; ?>
						</h2>
						<h2 class="faq-title-main">
							<?= (isset($args['faq_subtitle']) && $args['faq_subtitle']) ? $args['faq_subtitle'] : 'אנחנו כאן לענות על הכל!'; ?>
						</h2>
					</div>
				</div>
			</div>
			<div class="row justify-content-start">
				<div class="col-xl-9 col-lg-10 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-icon">?</span>
										<span class="faq-body-title"><?= $item['faq_question']; ?></span>
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output faq-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
