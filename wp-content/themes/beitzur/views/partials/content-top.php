<div class="padding-block">
	<div class="top-page" style="background-image: url('<?= (isset($args['img']) && $args['img']) ? $args['img'] : ''; ?>')">
		<div class="top-page-overlay">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col">
						<h2 class="top-title">
							<?= (isset($args['title']) && $args['title']) ? $args['title'] : ''; ?>
						</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
