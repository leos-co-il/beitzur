<?php if ($benefits = opt('benefits')) : ?>
	<div class="benefits-block">
		<div class="container">
			<?php if ($title = opt('benefits_title')) : ?>
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="with-line-title"><?= $title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($benefits as $benefit) : ?>
					<div class="col-xl-2 col-md-4 col-6">
						<div class="why-item">
							<div class="icon-wrap-why">
								<?php if ($benefit['ben_icon']) : ?>
									<img src="<?= $benefit['ben_icon']['url']; ?>" alt="benefit-icon">
								<?php endif; ?>
							</div>
							<h3 class="why-item-title">
								<?= $benefit['ben_title']; ?>
							</h3>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
