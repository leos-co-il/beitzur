<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-sm-6 col-12 col-post">
		<a class="post-card more-card" data-id="<?= $args['post']->ID; ?>" href="<?= $link; ?>">
			<div class="post-img" <?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>>
				<div class="card-content-wrap">
					<div class="post-card-content">
						<p class="post-small-text">
							<?= text_preview($args['post']->post_content, 10); ?>
						</p>
					</div>
					<div class="post-title-wrap">
						<h3 class="post-card-title font-weight-bold"><?= $args['post']->post_title; ?></h3>
					</div>
				</div>
			</div>
		</a>
	</div>
<?php endif; ?>
