<div class="repeat-form-block">
	<div class="repeat-form-overlay">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-12 col-md-10 col-12">
					<div class="wow zoomIn" data-wow-delay="0.2s">
						<?php if ($f_title = opt('base_form_title')) : ?>
							<h2 class="base-form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('base_form_subtitle')) : ?>
							<p class="base-form-subtitle"><?= $f_subtitle; ?></p>
						<?php endif;
						getForm('31'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
