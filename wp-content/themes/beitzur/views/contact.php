<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();
get_template_part('views/partials/content', 'top_page', [
	'title' => get_the_title(),
]); ?>
<article class="page-body contact-page">
	<?php get_template_part('views/partials/content', 'top', [
			'img' => $fields['top_img'] ? $fields['top_img']['url'] : IMG.'top-post.png',
			'title' => get_the_title(),
	]);
	?>
	<div class="white-back">
		<div class="container position-relative">
			<div class="row justify-content-center contact-content-row">
				<?php if ($fields['contact_form_title']) : ?>
					<div class="col-12">
						<h2 class="with-line-title">
							<?= $fields['contact_form_title']; ?>
						</h2>
					</div>
				<?php endif; ?>
				<div class="col-12">
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-12 pencil-relative-block">
					<img src="<?= IMG ?>pencil.png" alt="pencil" class="contact-pencil">
					<div class="form-contact-block wow zoomIn" <?php if (has_post_thumbnail()) : ?>
						style="background-image: url('<?= postThumb(); ?>')"
					<?php endif; ?>>
						<?php getForm('33'); ?>
					</div>
					<div class="pad-block-contact">
						<ul class="row contact-list justify-content-start align-items-start">
							<?php if ($tel = opt('tel')) : ?>
								<li class="contact-item col-lg col-12">
									<a href="tel:<?= $tel; ?>" class="contact-info-item">
									<span class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png">
									</span>
										<span class="contact-item-title"><?= 'טלפון: '.$tel; ?></span>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail = opt('mail')) : ?>
								<li class="contact-item col-lg col-12">
									<a href="mailto:<?= $mail; ?>" class="contact-info-item">
									<span class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png">
									</span>
										<span class="contact-item-title"><?= 'אימייל: '.$mail; ?></span>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($address = opt('address')) : ?>
								<li class="contact-item col-lg col-12">
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-item" target="_blank">
									<span class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-geo.png">
									</span>
										<span class="contact-item-title"><?= 'כתובת: '.$address; ?></span>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
