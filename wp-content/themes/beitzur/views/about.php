<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>


<article class="page-body">
	<?php get_template_part('views/partials/content', 'top', [
			'img' => $fields['top_img'] ? $fields['top_img']['url'] : IMG.'top-post.png',
			'title' => get_the_title(),
	]);
	?>
	<div class="container pt-5">
		<div class="row justify-content-center">
			<div class="col-12 post-content-col">
				<div class="page-about-image-wrap about-img-wrap">
					<?php if (has_post_thumbnail()) : ?>
						<div class="after-image">
							<img src="<?= postThumb(); ?>" alt="post-image" class="post-thumb-main">
						</div>
					<?php endif; ?>
				</div>
				<div class="base-output post-order-content about-content-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'benefits'); ?>
<div class="inverse-repeat-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_title_bold'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

