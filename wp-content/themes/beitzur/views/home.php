<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="main-block">
	<div class="padding-block">
		<div class="main-slider-wrap">
			<div class="main-slider" dir="rtl">
				<?php foreach ($fields['main_slider_img'] as $item) : ?>
					<div class="slide-main" style="background-image: url('<?= $item['url']; ?>')">
						<div class="slide-main-overlay"></div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="content-main-wrap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-10 col-12">
							<?php if ($logo_white = opt('logo_white')) : ?>
								<div class="row justify-content-center">
									<div class="col-md-6 col-8">
										<a href="/" class="logo-home">
											<img src="<?= $logo_white['url'] ?>" alt="logo">
										</a>
									</div>
								</div>
							<?php endif; ?>
							<div class="row justify-content-center">
								<div class="col-12">
									<?php if ($fields['h_main_title']) : ?>
										<h2 class="home-main-title"><?= $fields['h_main_title']; ?></h2>
									<?php endif; ?>
								</div>
								<?php if ($fields['h_main_link']) : ?>
									<div class="col-xl-4 col-auto">
										<a href="<?= $fields['h_main_link']['url']; ?>" class="home-main-link">
											<?= (isset($fields['h_main_link']['title']) && $fields['h_main_link']['title'])
													? $fields['h_main_link']['title'] : 'הדגמים שלנו';
											?>
										</a>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-lg-6 col-10">
							<?php if ($fields['h_gallery_links']) {
								get_template_part('views/partials/content', 'galleries',
										[
												'gals' => $fields['h_gallery_links'],
										]);
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
    <div class="row">
        <div class="col-12">

        </div>
    </div>
</div>
<div class="home-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-12">
				<?php getForm('30'); ?>
			</div>
		</div>
	</div>
</div>
<?php get_template_part('views/partials/repeat', 'benefits');
if ($fields['h_about_text'] || $fields['h_about_img']) : ?>
	<section class="home-about-block">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-xl-6 col-lg-6 col-md-10 col-12 d-flex flex-column align-items-start">
					<div class="base-output about-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<a href="<?= $fields['h_about_link']['url'];?>" class="base-link-reverse base-link-about">
							<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
									? $fields['h_about_link']['title'] : 'המשיכו קריאה';
							?>
						</a>
					<?php endif; ?>
				</div>
				<?php if($fields['h_about_img']) : ?>
					<div class="col-xl-5 col-lg-6 about-img-wrap">
						<div class="after-image">
							<img src="<?= $fields['h_about_img']['url']; ?>" alt="about-img">
						</div>
						<h3 class="about-years">30 שנות</br>
							ניסיון</h3>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_projects']) : ?>
	<section class="home-posts white-back">
		<div class="container">
			<?php if ($fields['h_projects_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="with-line-title">
							<?= $fields['h_posts_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch arrows-slider">
				<div class="col-12 col-arrows">
					<div class="projects-slider" dir="rtl">
						<?php foreach ($fields['h_projects'] as $i => $project) : $link = get_the_permalink($project); ?>
							<div class="prod-slide">
								<a class="post-card more-card" href="<?= $link; ?>">
									<div class="post-img" <?php if (has_post_thumbnail($project)) : ?>
										style="background-image: url('<?= postThumb($project); ?>')" <?php endif; ?>>
										<div class="card-content-wrap">
											<div class="post-card-content">
												<p class="post-small-text">
													<?= text_preview($project->post_content, 10); ?>
												</p>
											</div>
											<div class="post-title-wrap">
												<h3 class="post-card-title font-weight-bold"><?= $project->post_title; ?></h3>
											</div>
										</div>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($fields['h_projects_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['h_projects_link']['url']; ?>" class="base-link">
							<?= (isset($fields['h_projects_link']['title']) && $fields['h_projects_link']['title']) ?
									$fields['h_projects_link']['title'] : 'לכל הפרוייקטים שלנו'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form');
if ($fields['h_posts']) : ?>
	<section class="home-posts white-back">
		<div class="container">
			<?php if ($fields['h_posts_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="with-line-title">
							<?= $fields['h_posts_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_posts'] as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
			<?php if ($fields['h_posts_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['h_posts_link']['url']; ?>" class="base-link">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title']) ?
									$fields['h_posts_link']['title'] : 'לכל המאמרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_slider_seo']) : ?>
	<section class="dark-home-slider">
		<?php get_template_part('views/partials/content', 'slider',
				[
						'content' => $fields['h_slider_seo'],
						'img' => $fields['h_slider_img'],
				]); ?>
	</section>
<?php endif;
if ($fields['reviews_gallery']) : ?>
	<section class="reviews-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-11 col-12">
					<div class="reviews-back d-flex flex-column justify-content-between align-items-center">
						<?php if ($fields['reviews_title']) : ?>
							<h2 class="reviews-title"><?= $fields['reviews_title']; ?></h2>
						<?php endif; ?>
						<div class="base-slider" dir="rtl">
							<?php foreach ($fields['reviews_gallery'] as $review) : ?>
								<div>
									<a class="review-item" href="<?= $review['url']; ?>" data-lightbox="reviews">
										<img src="<?= $review['url']; ?>" alt="review">
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_products']) : ?>
	<section class="home-products">
		<div class="container">
			<?php if ($fields['h_products_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="with-line-title with-line-white">
							<?= $fields['h_products_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_products'] as $i => $post) {
					get_template_part('views/partials/card', 'product', [
							'post' => $post,
					]);
				} ?>
			</div>
			<?php if ($fields['h_products_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['h_products_link']['url']; ?>" class="base-link base-link-reverse">
							<?= (isset($fields['h_products_link']['title']) && $fields['h_products_link']['title']) ?
									$fields['h_products_link']['title'] : 'לכל המוצרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_title_bold'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

