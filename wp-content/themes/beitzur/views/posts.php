<?php
/*
Template Name: מאמרים/פרויקטים/מוצרים
*/

get_header();
$fields = get_fields();
$post_type = $fields['type'] ? $fields['type'] : 'post';
$posts = new WP_Query([
		'posts_per_page' => 8,
		'post_type' => $post_type,
		'suppress_filters' => false
]);
$published_posts = '';
$count_posts = wp_count_posts($post_type);
if ( $count_posts ) {
	$published_posts = $count_posts->publish;
}
?>

<article class="page-body mb-4 blog-body">
	<?php get_template_part('views/partials/content', 'top', [
			'img' => $fields['top_img'] ? $fields['top_img']['url'] : IMG.'top-gallery.png',
			'title' => get_the_title(),
	]);
	?>
	<div class="container">
		<div class="row justify-content-center">
			<?php if ($fields['title']) : ?>
				<div class="col-12">
					<h1 class="with-line-title with-line-white"><?= $fields['title']; ?></h1>
				</div>
			<?php endif; ?>
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					if ($post_type == 'product') {
						get_template_part('views/partials/card', 'product',
								[
										'post' => $post,
								]);
					} else {
						get_template_part('views/partials/card', 'post',
								[
										'post' => $post,
								]);
					}
				} ?>
			</div>
		<?php endif;
		if ($published_posts && $published_posts > 8) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="<?= $post_type; ?>">
						<img src="<?= ICONS ?>arrow-down.png" alt="load-more">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="inverse-repeat-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>

