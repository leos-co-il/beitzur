<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$title_f = opt('pro_form_title');
$subtitle_f = opt('pro_form_subtitle');
$product_thumb = has_post_thumbnail() ? postThumb() : '';
?>

<article class="page-body">
	<?php get_template_part('views/partials/content', 'top', [
		'img' => $fields['top_img'] ? $fields['top_img']['url'] : IMG.'top-project.png',
		'title' => get_the_title(),
	]);
	?>
	<div class="container-fluid pt-5 mb-4">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-10 col-sm-11 col-12">
				<div class="row justify-content-between">
					<div class="col-xl-5 col-lg-6">
						<div class="base-output project-output">
							<?php the_content(); ?>
						</div>
						<div class="project-form-wrap" data-id="<?= get_the_ID(); ?>" data-link="<?= $post_link; ?>">
							<?php if ($title_f || $subtitle_f) : ?>
								<div class="d-flex flex-wrap align-items-center mb-2">
									<h3 class="pro-form-title my-1">
										<?= $title_f; ?>
									</h3>
									<h3 class="pro-form-title font-weight-normal m-1">
										<?= $subtitle_f; ?>
									</h3>
								</div>
							<?php endif;
							getForm('34'); ?>
						</div>
						<div class="socials-share">
							<span class="pro-form-title">
								שתפו את הפרוייקט!
							</span>
							<!--	MAIL-->
							<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
							   class="social-share-link">
								<img src="<?= ICONS ?>share-mail.png">
							</a>
							<!--	WHATSAPP-->
							<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
								<img src="<?= ICONS ?>share-whatsapp.png">
							</a>
							<!--	FB -->
							<a href="<?php the_title(); echo $post_link; ?>" class="social-share-link">
								<img src="<?= ICONS ?>share-messenger.png">
							</a>
						</div>
					</div>
					<?php if ($product_thumb && $fields['pro_gallery']) : ?>
						<div class="col-lg-6 mb-5">
							<div class="gallery-slider-wrap">
								<div class="thumbs" dir="rtl">
									<div class="pb-1">
										<div class="thumb-item" style="background-image: url('<?= $product_thumb; ?>')">
											<a class="gallery-trigger" href="<?= $product_thumb; ?>" data-lightbox="images">
												+
											</a>
										</div>
									</div>
									<?php foreach ($fields['pro_gallery'] as $img): ?>
										<div class="pb-1">
											<div class="thumb-item"  style="background-image: url('<?= $img['url']; ?>')">
												<a class="gallery-trigger" href="<?= $img['url']; ?>" data-lightbox="images">
													+
												</a>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="gallery-slider" dir="rtl">
									<?php if($product_thumb): ?>
										<div class="px-1 pb-1">
											<div class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')">
												<a class="gallery-trigger" href="<?= $product_thumb; ?>" data-lightbox="images">
													+
												</a>
											</div>
										</div>
									<?php endif;
									foreach ($fields['pro_gallery'] as $img): ?>
										<div class="px-1">
											<div class="big-slider-item" style="background-image: url('<?= $img['url']; ?>')">
												<a class="gallery-trigger" href="<?= $img['url']; ?>" data-lightbox="images">
													+
												</a>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php elseif ($product_thumb && !$fields['pro_gallery']) : ?>
						<div class="col-lg-6 col-12 mb-5">
							<img src="<?= $product_thumb; ?>" alt="product-img" class="w-100">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'product',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_prods']) {
	$samePosts = $fields['same_prods'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'product',
		'post__not_in' => array($postId),
	]);
}
$link_all = $fields['same_prod_link'] ? $fields['same_prod_link'] : opt('same_products_link');
if ($samePosts) : ?>
	<section class="same-posts">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="with-line-title with-line-white">
						<?= $fields['same_title'] ? $fields['same_title'] : 'מוצרים דומים שעושיים לעניין אותך'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $i => $post) {
					get_template_part('views/partials/card', 'product', [
						'post' => $post,
					]);
				} ?>
			</div>
			<?php if ($link_all) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $link_all['url']; ?>" class="base-link base-link-reverse border-link-gold">
							<?= (isset($link_all['title']) && $link_all['title']) ?
								$link_all['title'] : 'לכל המוצרים שלנו'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'subtitle' => $fields['faq_title_bold'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
