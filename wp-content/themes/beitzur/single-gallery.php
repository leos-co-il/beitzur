<?php

the_post();
get_header();
$fields = get_fields();
$galleries = get_posts([
		'posts_per_page' => -1,
		'post_type' => 'gallery',
]);
?>

<article class="page-body pb-5">
	<?php get_template_part('views/partials/content', 'top', [
		'img' => $fields['top_img'] ? $fields['top_img']['url'] : IMG.'top-gallery.png',
		'title' => get_the_title(),
	]);
	?>
	<div class="container" id="success-block">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="gallery-page-title">
					<?= (isset($fields['title']) && $fields['title']) ? $fields['title'] : 'איזה סוג מטבח תרצו?'; ?>
				</h1>
			</div>
			<?php if ($galleries) : ?>
				<div class="col-xl-6 col-lg-8 col-12 mb-4">
					<?php get_template_part('views/partials/content', 'galleries',
						[
								'gals' => $galleries,
						]); ?>
				</div>
			<?php endif; ?>
			<div class="col-12 mb-4">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
			<?php if ($fields['gallery_post_gallery']) : $gals_7 = array_chunk($fields['gallery_post_gallery'], 7); ?>
				<?php foreach ($gals_7 as $chunk_num => $gal_chunk) : ?>
				<div class="col-12 gallery-chunk" data-chunk="<?= $chunk_num; ?>">
					<div class="row">
						<?php $arr_first = array_slice($gal_chunk, 0, 2);
						$arr_mid_res = [];
						$arr_last = [];
						if (isset($gal_chunk['2'])) {
							$arr_mid = array_slice($gal_chunk, 2, 3); $arr_mid_res = array_chunk($arr_mid, 2);
						} elseif (isset($gal_chunk['5'])) {
							$arr_last = array_slice($gal_chunk, 5, 2);
						}
						foreach ($arr_first as $z => $img) : ?>
							<div class="col-6 mb-4">
								<div class="gallery-image" style="background-image: url('<?= $img['url']; ?>')">
									<a href="<?= $img['url']; ?>" data-lightbox="image" class="gallery-trigger">+</a>
								</div>
							</div>
						<?php endforeach;
						if ($arr_mid_res) : foreach ($arr_mid_res as $x => $arr_middle) : ?>
							<div class="<?= ($x == 0) ? 'col-4 mb-3' : 'col-8 mb-2'; ?>">
								<?php foreach ($arr_middle as $y => $img) : ?>
									<div class="gallery-image mb-4" style="background-image: url('<?= $img['url']; ?>')">
										<a href="<?= $img['url']; ?>" data-lightbox="image" class="gallery-trigger">+</a>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endforeach; endif;
						if ($arr_last) : foreach ($arr_last as $z => $img) : ?>
							<div class="col-6 mb-4">
								<div class="gallery-image" style="background-image: url('<?= $img['url']; ?>')">
									<a href="<?= $img['url']; ?>" data-lightbox="image" class="gallery-trigger">+</a>
								</div>
							</div>
						<?php endforeach; endif; ?>
					</div>
				</div>
				<?php endforeach; ?>
				<?php if (count($gals_7) > 1) : ?>
					<div id="load-more-items" class="more-link">
						<img src="<?= ICONS ?>arrow-down.png" alt="load-more">
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</article>
<div class="inverse-repeat-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'subtitle' => $fields['faq_title_bold'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
