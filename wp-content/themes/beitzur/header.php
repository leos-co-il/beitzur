<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
    <div class="container-fluid">
        <div class="row justify-content-between align-items-center">
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-auto">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
            <div class="col">
            	<nav id="MainNav" class="h-100">
                    <div id="MobNavBtn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
                </nav>
            </div>
			<?php if ($tel = opt('tel')) : ?>
				<div class="col-auto">
					<a href="tel:<?= $tel; ?>" class="header-tel">
						<img src="<?= ICONS ?>form-tel.png" alt="tel">
						<span class="tel-number"><?= $tel; ?></span>
					</a>
				</div>
			<?php endif; ?>
        </div>
    </div>
</header>
<div class="triggers-col">
	<?php $facebook = opt('facebook');
	$instagram = opt('instagram');
	$whatsapp = opt('whatsapp');
	if ($whatsapp) : ?>
	<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link-item"
	   target="_blank">
		<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
	</a>
	<?php endif;
	if ($instagram) : ?>
	<a href="<?= $instagram; ?>" class="social-link-item">
		<img src="<?= ICONS ?>instagram.png" alt="instagram">
	</a>
	<?php endif;
	if ($facebook) : ?>
		<a href="<?= $facebook; ?>" class="social-link-item">
			<img src="<?= ICONS ?>facebook.png" alt="facebook">
		</a>
	<?php endif; ?>
	<div class="pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png" alt="pop-trigger">
	</div>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-12 d-flex justify-content-center">
				<div class="float-form d-flex flex-column align-items-center">
					<span class="close-form">
						<img src="<?= ICONS ?>close.png" alt="close-popup">
					</span>
					<?php if ($f_title = opt('pop_form_title')) : ?>
						<h2 class="pop-form-title"><?= $f_title; ?></h2>
					<?php endif;
					if ($f_subtitle = opt('pop_form_subtitle')) : ?>
						<p class="pop-form-subtitle"><?= $f_subtitle; ?></p>
					<?php endif;
					getForm('29'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
